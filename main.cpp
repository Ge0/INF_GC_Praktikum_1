/* Computergrafik Praktikum
 * Klassisches OpenGL mit freeGLUT
 * FH Aachen, 2015
 */

#include <GL/glut.h>

#define WINDOW_CAPTION "Computergrafik Praktikum"
#define WINDOW_XPOS 50
#define WINDOW_YPOS 50
#define WINDOW_WIDTH 480
#define WINDOW_HEIGHT 480
#define UPDATE_RATE 60


// global variable for engine purposes
// not pretty and hardware-dependent, but gets the job done
unsigned int counter = 0;


// OpenGL settings initialization
void init()
{
    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);//Disabled by default
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);//glShadeModel(GL_FLAT); Flat only picks the last color of vertices
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glClearDepth(1.0f);
    glClearColor(.0f, .0f, .0f, .0f);//Set bgColor black
}


// Handler for timer event
void update(int v)
{
    // Schedule a draw event
    glutPostRedisplay();

    // Needs to be registered again
    glutTimerFunc((unsigned int) 1000.0f / UPDATE_RATE, update, v);
}


// Handler for window resize and creation event
void reshape(GLsizei width, GLsizei height)
{
   // Compute aspect ratio
   height = (height == 0) ? 1 : height;
   GLfloat aspect = (GLfloat)width / (GLfloat)height;

   // Set viewport to cover the whole window
   glViewport(0, 0, width, height);

   // Set projection matrix to a perspective projection
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   //gluPerspective(45.0f, aspect, 0.1, 100.0);//Perspective view
   glOrtho(-2.0,2.0,-2.0,2.0,0.1,10.0); //orthogonal view
}


// Handler for window draw event
void draw()
{
    // Clear buffer to set color and alpha
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Apply model view transformations
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -7.0f);

    // Set color for drawing
    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

    // Rotate shape
    glRotatef(45 + counter,0,0,1);//rotation of counter° around z axis
    glRotatef(180,0,1,0);// rotation of 180° around y axis to see the back of the object
    // Draw shape
    float x = .5f, y = .5f, z = .5f;

    glBegin(GL_TRIANGLES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f( 1.0f + x, -1.0f + y,  0.0f + z);//Slight modification of he location of the triangle
        glColor3f(0.0f, 1.0f, 0.0f);
        glVertex3f( 1.0f + x,  1.0f + y,  0.0f + z);
        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(-1.0f + x,  1.0f + y,  0.0f + z);
    glEnd();
    //adds second triangle to make a square
    glBegin(GL_TRIANGLES);
        glColor3f(1.0f, 0.0f, 0.0f);
        glVertex3f( 1.0f + x, -1.0f + y,  0.0f + z);
        glColor3f(0.0f, 0.0f, 1.0f);
        glVertex3f(-1.0f + x,  1.0f + y,  0.0f + z);
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex3f(-1.0f + x, -1.0f + y,  0.0f + z);
    glEnd();

    // Execute all issued GL commands
    glFlush(); // replace with glutSwapBuffers() for double buffered mode

    // Increment counter
    counter++;
}


// main function
int main(int argc, char **argv)
{
    // initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_SINGLE); // GLUT_DOUBLE for double buffered mode

    // Create output window
    glutInitWindowPosition(WINDOW_XPOS, WINDOW_YPOS);
    glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    glutCreateWindow(WINDOW_CAPTION);

    // Register callback handlers for GLUT events
    glutReshapeFunc(reshape);
    glutDisplayFunc(draw); // Note: not called continuously
    glutTimerFunc((unsigned int) 1000.0f / UPDATE_RATE, update, 0); // Note: inaccurate, but good enough here

    // Initialize OpenGL parameters
    init();

    // Jump into the main drawing loop
    glutMainLoop();

    return 0;
}
